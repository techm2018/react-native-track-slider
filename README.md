## React Native Track Slider

[![4-BC7-FAA3-2-FAC-4567-A9-ED-BC53733-AEB59.jpg](https://i.postimg.cc/Jh7GCBXx/4-BC7-FAA3-2-FAC-4567-A9-ED-BC53733-AEB59.jpg)](https://postimg.cc/w1P6hM0R)

# Track Slider

Slider which responds to slide input and shows the tracks the current value of the slider

# Static Slider

Display only copy of the above slider for View Screens.

---

# Example

React Native Code

```
import Slider from 'react-native-track-slider';
import StaticSlider from 'react-native-track-slider';


<Slider initialValue={35} minimumValue={10} maximumValue={90} trackColor='#730f5e' 
		trackBackgroundColor='gray' trackBubbleColor='#730f5e' sliderLabelColor='#95a0b8' />

<StaticSlider minimumValue={1} maximumValue={100} staticValue={45} trackColor='#3377FF' 
        trackBackgroundColor='#DAE4AB' trackBubbleColor='#3377FF' sliderLabelColor='#DBDFDD' />

```
