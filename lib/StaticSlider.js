import React, { useState } from "react";
import { Dimensions, StyleSheet, View, Text, Platform } from "react-native";
import PropTypes from 'prop-types';

const { widthDimensions } = Dimensions.get('window');

const RULER_HEIGHT = 10;
const KNOB_SIZE = 75;

const StaticSlider = ({ staticValue, minimumValue, maximumValue, trackColor, trackBackgroundColor, trackBubbleColor, sliderLabelColor }) => {

  const [screenWidth, setScreenWidth] = useState(250);
  const leftSliderWidth = (staticValue > minimumValue) ? (((staticValue * screenWidth) / (maximumValue - minimumValue)) - KNOB_SIZE/2) : 0;
  const rightSliderWidth = screenWidth - leftSliderWidth - (KNOB_SIZE/2) ;
  console.log('Static slider - ' + screenWidth + '-left: '+ leftSliderWidth)

  const backgroundSliderStyle = {
    backgroundColor: trackBackgroundColor, 
  }

  const trackKnobStyle = {
    borderColor: trackBubbleColor,
  }

  const trackBubbleStyle = {
    backgroundColor: trackBubbleColor,
  }

  const trackBubbleBorderStyle = {
    borderBottomColor: trackBubbleColor,
  }

  const sliderLabelStyle= {
    color: sliderLabelColor,
  }

  const sliderWidthStyle = {
    width: screenWidth,
  }

  const sliderKnobPositionStyle = {
    left: leftSliderWidth+10,
  }

  return (
    <View style={[styles.slider, sliderWidthStyle]} onLayout={(x) => {
        const { width: layoutWidth, height: layoutHeight } = x.nativeEvent.layout;
        setScreenWidth (layoutWidth);
      }}>
        <View>
          <View style={[styles.backgroundSlider, backgroundSliderStyle]} />
          <View style={styles.sliderLabelRow}>
            <Text style={[styles.sliderLabel, sliderLabelStyle]}>{minimumValue}</Text>
            <Text style={[styles.sliderLabel, sliderLabelStyle]}>{maximumValue}</Text>
          </View>
    	</View>

      <View
          style={[
            styles.backgroundSlider,
            {
              ...StyleSheet.absoluteFillObject,
              backgroundColor: trackColor,
              right: rightSliderWidth,
            },
          ]}
      />

      <View style={[styles.sliderKnobContainer, sliderKnobPositionStyle]}>
        <View style={{position: 'absolute',backgroundColor: 'transparent'}}>
          <View style={[styles.sliderKnob, trackKnobStyle]} />
          <View style={[styles.talkBubbleSquare, trackBubbleStyle]} ><Text style={{color: '#fff'}}>{staticValue}</Text></View>
          <View style={[styles.talkBubbleTriangle, trackBubbleBorderStyle]} />
        </View>
      </View>

    </View>
  );

};

const styles = StyleSheet.create({
  slider: {
    height: RULER_HEIGHT,
    marginTop: 10,
    marginBottom: 55,
  },
  backgroundSlider: {
    height: 5,
    backgroundColor: 'gray',
    marginLeft: 5,
  },
  sliderLabelRow: {
    flexDirection: 'row', 
    justifyContent: 'space-between',
  },
  sliderLabel: {
    fontSize: 14
  },
  sliderKnobContainer: {
  	top: Platform.OS === 'ios' ? -20: -28,
  },
  sliderKnob: {
  	left: 16,
    width: 15,
    height: 15,
    borderRadius: 15/2,
    borderWidth: 3,
    backgroundColor: 'white',
  },
  talkBubbleSquare: {
    top: 13,
    width: 50,
    height: 30,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  talkBubbleTriangle: {
    position: 'absolute',
    width: 0,
    height: 0,
    left: 14,
    top: 13,
    borderTopColor: 'transparent',
    borderLeftWidth: 10,
    borderRightWidth: 10,
    borderBottomWidth: 15,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
  },
});

export default StaticSlider;